const novaCobranca = require('../model/Cobranca')
const axios = require('axios')
const {getCiclistaInfo} = require("../model/Ciclista")
const { filaCobrancasBd } = require("../model/Cobranca")
const { routes } = require("../routes")
const {cartoesModel} = require('../model/CartaoDeCredito')


const cobrancaController = {
    cobranca: async (req, res) => {  
        const { id, valor } = req.body
        const ciclistaInfo = getCiclistaInfo(id)
        

        const index = filaCobrancasBd.findIndex(cobranca => cobranca.id === id)

        filaCobrancasBd[index] = { ...filaCobrancaBd[index], status: "Concluida", dataPagamento: new Date().toISOString()  }

        const textEmail = `Bom dia ${ciclistaInfo.nome}, esperamos que essa mensagem lhe encontre bem, uma cobrança atrasada de ${valor} acabou
        de ser quitada, Segue dados:${filaCobrancaBd[index]} `

        axios.post('https://http://localhost:3001/enviaremail', {
            "mensagem": textEmail,
            "email": ciclistaInfo.email,
            "assunto":"Cobrança atrasada quitada Do Mês"
        }).then((response)).catch((error) => {
            res.status(500).json({ msg: "Erro ao processar a cobrança" });
        })

        res.status(200).json({ msg: "Cobrança processada" });

    },

    processaCobrancasEmFila: async (req, res) => {
        for (let i = 1; i < filaCobrancasBd.length; i++){
            await axios.get(`https://http://localhost:3001/getCobranca/${i}`).then(async (cobranca) => {
                cobranca = cobranca.data
                if (cobranca.status == "Pendente") {
                    await axios.post('https://http://localhost:3001/cobranca', {
                        "valor":cobranca.valor,
                        "id":cobranca.id 
                    }).then((response) => {
                         res.status(200).json({ msg: "Cobrança processada" });
                    }).catch((error) => {
                        res.status(500).json({ msg: "Erro ao processar a fila de cobrança" });
                    })
                }
                
            }).catch((error) => {
                res.status(500).json({ msg: "Erro ao processar a a fila de cobrança" });
            })
        }
        res.status(200).json({'msg':"Sucesso"})

    },

    getCobranca: async (req, res) => {
        const { id } = req.params 
        let cobranca = null
        
        try {
            cobranca = filaCobrancasBd.find((cobranca) => cobranca.id === parseInt(id));
        } catch (error) {
            console.log(error);
        }
        

        if (cobranca != null) {
            return res.status(200).json({ msg:cobranca});
        } else {
            return res.status(404).json({ msg:"Nao encontrado"});
        }

    },

    filaCobranca: async (req, res) => {
        try {
            const { valor, ciclista } = req.body;
            const horaAtual = new Date().toISOString();
            const novaCobranca = {
                id: filaCobrancasBd.length + 1,
                status: "Pendente",
                horaSolicitacao: horaAtual,
                horaFinalizacao: horaAtual,
                valor,
                ciclista,
            };

    
            filaCobrancasBd.push(novaCobranca);
            res.status(200).json({ msg: "Cobrança adicionada com sucesso", cobranca: novaCobranca });
        } catch (erro) {
            console.error("Erro ao processar a cobrança:", erro);
            res.status(500).json({ msg: "Erro ao processar a cobrança" });
        }
    },

    alterarCartaoDeCredito: async (req, res) => {
        const { numero, nomeTitular, validade, cvv, IdDoCiclista } = req.body
        
        axios.post("https://http://localhost:3001/validaCartaoDeCredito",
            numero).then((response) => {
                if (response.status === 200) {
                    const cartaoDoCiclista = cartoesModel.findIndex(cartao => cartao.IdDoCiclista === IdDoCiclista);
                    cartoesModel[cartaoDoCiclista] = { nomeTitular, numero, validade, cvv }
                    
                    return res.status(200).json({msg:"Sucesso, cartão atualizado" })
                } else {
                    return res.status(422).json({msg:"Dados Inválidos"})
                }
            }).catch((error) => {
                return res.status(422).json({msg:"Erro na requisição"})
            })
        
    }



,
    validaCartaoDeCredito: async (req, res) => {
        const {numeroCartao} = req.body

        const url = `https://lookup.binlist.net/${numeroCartao}`;

        try {
            const response = await axios.get(url);

            if (response.data.bank) {
                return res.status(200).json({msg:"Dados validados"})
            }  
            
        } catch (error) {
            console.error('Erro ao validar o cartão:', error.message);
            return res.status(422).json({msg:"Dados Inválidos"})
        }
        return res.status(422).json({msg:"Dados Inválidos"})
    
    }
}


module.exports = cobrancaController