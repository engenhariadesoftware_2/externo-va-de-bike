const regExp = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/
const novoEmail = require('../model/Email') 
const {mailOptions,transporter} = require('../nodeMailerConfig')

const emailController = {
    enviarEmail: async (req, res) => {
        const { mensagem, email, assunto } = req.body
        try {
            if (!email || !mensagem) {
                return res.status(404).json({ msg: "Erro,email não existe" })
            }
            if (!regExp.test(email)) {
                return res.status(422).json({ msg: "Erro,email com formato inválido" })
            }
        } catch (erro) {
            console.log(erro)
        }
        
        return res.status(200).json({ msg: "Sucesso,externo solicitado" })


        //simula o envio
        // const mailOptions = {
        //     from: 'vadebike@gmail.com', 
        //     to: email, 
        //     subject: assunto,
        //     text: mensagem,
        // };

        
    }
}

module.exports = emailController


