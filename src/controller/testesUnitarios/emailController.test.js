const emailController = require("../emailController")

describe('emailController - enviarEmail', () => {
  it('O email deve ser enviado com sucesso', async () => {
    const req = {
      body: {
        mensagem: 'Seja bem vindo a va de bike',
        email: 'leojardim5@gmail.com',
        assunto: 'Assinatura',
      },
    };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
      
    await emailController.enviarEmail(req, res);
      
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledWith({ msg: 'Sucesso,externo solicitado' });
  });

  it('deve retornar um erro para e-mail inválido', async () => {
    const req = {
      body: {
        mensagem: 'Seja bem vindo a va de bike',
        email: 'email_mal_formatado',
        assunto: 'Assinatura',
      },
    };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    await emailController.enviarEmail(req, res);
    expect(res.status).toHaveBeenCalledWith(422);
    expect(res.json).toHaveBeenCalledWith({ msg: 'Erro,email com formato inválido' });
  });
  
  it('deve retornar um erro para a mensagem vazia', async () => {
    const req = {
      body: {
        mensagem: null,
        email: 'test@example.com',
        assunto: 'Teste de Assunto',
      },
    };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    await emailController.enviarEmail(req, res);
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledWith({ msg: 'Erro,email não existe' });
  });
});