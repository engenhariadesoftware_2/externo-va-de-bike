const cobrancaController = require('../cobrancaController')

describe('cobrancaController', () => {
    describe('cobranca', () => {
        test('deve processar a cobrança e enviar um email com sucesso', async () => {
            const req = {
                body: {
                    ciclista: 1,
                    valor: 100,
                },
            }
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            }
            await cobrancaController.cobranca(req, res)

            expect(res.status).toHaveBeenCalledWith(200)
            expect(res.json).toHaveBeenCalledWith({ msg: 'Cobrança processada' })
        })
        test('deve retornar erro ao processar a cobrança para usuário inválido ou nao conseguir enviar o  email', async () => {
            const req = {
                body: {
                    ciclista: 'nomeEmVez',
                    valor: 100,
                },
            }
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            }
            await cobrancaController.cobranca(req, res)
            expect(res.status).toHaveBeenCalledWith(500)
            expect(res.json).toHaveBeenCalledWith({ msg: 'Erro ao processar a cobrança' })
        })
    })

    describe('processaCobrancasEmFila', () => {
        test('deve processar a fila com sucesso', async () => {
            const filaCobrancasBd = [
                { id: 1, status: 'Pendente', valor: 50, ciclista: 'Julia' },
                { id: 2, status: 'Pendente', valor: 75, ciclista: 'Leonardo' },
            ]
            const req = {}
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            }
            await cobrancaController.processaCobrancasEmFila(req, res)

            expect(res.status).toHaveBeenCalledWith(200)
            expect(res.json).toHaveBeenCalledWith({ msg: 'Sucesso' })
        })
        test('deve retornar erro ao processar fila vazia', async () => {
            const filaCobrancasBd = []
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            }
            req = {}

            await cobrancaController.processaCobrancasEmFila(req, res)

            expect(res.status).toHaveBeenCalledWith(500)
            expect(res.json).toHaveBeenCalledWith({ msg: 'Erro ao processar a fila de cobrança' })
        })
    })
  
    describe('getCobranca', () => {
        test('deve retornar uma cobrança existente', async () => {
            const filaCobrancasBd = [
                { id: 1, status: 'Pendente', valor: 50, ciclista: 'Gabriel' },
                { id: 2, status: 'Pendente', valor: 75, ciclista: 'Patrick' },
            ]

            const req = {
                params: { id: 1 },
            }
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            }

            await cobrancaController.getCobranca(req, res)

            expect(res.status).toHaveBeenCalledWith(200)
            expect(res.json).toHaveBeenCalledWith({ msg: filaCobrancasBd[0] })
        })

        test('deve retornar erro se a cobrança não existir', async () => {
            const filaCobrancasBd = []

            const req = {
                params: { id: 1 },
            }

            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            }

            await cobrancaController.getCobranca(req, res)

            expect(res.status).toHaveBeenCalledWith(404)
            expect(res.json).toHaveBeenCalledWith({ msg: 'Nao encontrado' })
        })
    })
  
    describe('filaCobranca', () => {
        test('deve adicionar uma cobrança à fila com sucesso', async () => {
            const req = {
                body: { valor: 120, ciclista: 'novoUsuario' },
            }

            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            }

            await cobrancaController.filaCobranca(req, res)

            expect(res.status).toHaveBeenCalledWith(200)
            expect(res.json).toHaveBeenCalledWith({ msg: 'Cobrança adicionada com sucesso', cobranca: expect.any(Object) })
        })

        test('deve retornar erro ao adicionar cobrança à fila com valor ausente', async () => {
            const req = {
                body: { ciclista: 'novoUsuario' },
            }

            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            }

            await cobrancaController.filaCobranca(req, res)

            expect(res.status).toHaveBeenCalledWith(500)
            expect(res.json).toHaveBeenCalledWith({ msg: 'Erro ao processar a cobrança' })
        })
    })

    describe('validaCartaoDeCredito', () => {
        test('deve validar um cartão de crédito com sucesso', async () => {
            const req = {
                body: { numeroCartao: '4111111111111111' },
            }
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            }
            await cobrancaController.validaCartaoDeCredito(req, res)

            expect(res.status).toHaveBeenCalledWith(200)
            expect(res.json).toHaveBeenCalledWith({ msg: 'Dados atualizados' })
        })

        test('deve retornar erro ao validar um cartão de crédito inválido', async () => {
            const req = {
                body: { numeroCartao: 'invalido' },
            }
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            }
            await cobrancaController.validaCartaoDeCredito(req, res)
            expect(res.status).toHaveBeenCalledWith(422)
            expect(res.json).toHaveBeenCalledWith({ msg: 'Dados Inválidos' })
        })
    })
})