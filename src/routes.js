const express = require('express')
const emailController = require('./controller/emailController')
const cobrancaController = require('./controller/cobrancaController')
const routes = express()


routes.post('/validaCartaoDeCredito', cobrancaController.validaCartaoDeCredito)
routes.post('/filacobranca', cobrancaController.filaCobranca)
routes.get('/getCobranca/:id', cobrancaController.getCobranca)
routes.post('/processaEmFila', cobrancaController.processaCobrancasEmFila)
routes.post('/cobranca', cobrancaController.cobranca)
routes.post('/enviaremail', emailController.enviarEmail)

module.exports = routes

